/*jslint node: true */
/*global describe:true, it: true, beforeEach:true, afterEach:true */
"use strict";

var assert = require('assert');
var fs = require('fs');
var path = require('path');
var util = require('util');
var marked = require('../..');

function getDefaultOptions() {
  return { gfm: true, tables: true, sanitize: true, breaks: true, linkify: true };
}


describe('mentions', function() {

  [{
    name: 'should deal with mentions followed by commas',
    input: '@suprememoocow, how are you?',
    expectedMention: 'suprememoocow'
  },{
    name: 'should deal with mentions followed by colons',
    input: '@suprememoocow: how are you?',
    expectedMention: 'suprememoocow'
  }, {
    name: 'should deal with numbers in usernames',
    input: '@123456789, how are you?',
    expectedMention: '123456789'
  }, {
    name: 'should deal with underscores in usernames',
    input: '@123456789_twitter, how are you?',
    expectedMention: '123456789_twitter'
  }, {
    name: 'should deal with dots in usernames',
    input: '@Demo.user_gitlab, how are you?',
    expectedMention: 'Demo.user_gitlab'
  }, {
    name: 'should deal with mentions followed by dots',
    input: "That's a job for @Demo.user_gitlab.",
    expectedMention: 'Demo.user_gitlab'
  },
  // https://matrix.org/docs/spec/appendices#user-identifiers
  {
    name: 'should deal with virtualUser mentions',
    input: "That's a job for @madlittlemods:matrix.org and their team.",
    expectedMention: 'madlittlemods:matrix.org'
  }, {
    name: 'should deal with virtualUser mentions followed by dots',
    input: "That's a job for @madlittlemods:matrix.org.",
    expectedMention: 'madlittlemods:matrix.org'
  },
  // https://matrix.org/docs/spec/appendices#server-name
  {
    name: 'should deal with virtualUser mention with IP server_name followed by dots',
    input: "That's a job for @madlittlemods:1.2.3.4:1234.",
    expectedMention: 'madlittlemods:1.2.3.4:1234'
  },
  {
    name: 'should deal with virtualUser mention with IPv6 literal',
    input: "That's a job for @madlittlemods:[1234:5678::abcd] and their team.",
    expectedMention: 'madlittlemods:[1234:5678::abcd]'
  }
  // This test does not work with the current regex
  /* * /
  {
    name: 'should deal with virtualUser mention with IPv6 literal followed by dots',
    input: "That's a job for @madlittlemods:[1234:5678::abcd].",
    expectedMention: 'madlittlemods:[1234:5678::abcd]'
  }
  /* */
  ].forEach((fixture) => {
    it(fixture.name, function() {
      var options = getDefaultOptions();
  
      var lexer = new marked.Lexer(options);
      var mentions = 0;
      var renderer = new marked.Renderer();
  
      renderer.mention = function(href, title, text) {
        mentions++;
        assert.strictEqual(href, fixture.expectedMention);
        assert.strictEqual(text, `@${fixture.expectedMention}`);
      };
  
      var tokens = lexer.lex(fixture.input);
      options.renderer = renderer;
  
      var parser = new marked.Parser(options);
      var html = parser.parse(tokens);
  
      assert.equal(mentions, 1);
    });
  });
});
